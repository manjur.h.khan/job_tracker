from boto3 import resource
from hashlib import sha512
from time import time
from boto3.dynamodb.conditions import Key, Attr
from os import urandom
from random import randint

def lambda_handler(event, context):
    email = event["email"]
    password = event["password"]
    
    password = sha512(password.encode()).hexdigest()
    
    dynamodb = resource('dynamodb')
    table = dynamodb.Table('JobTracker01')
    
    response = table.query(
        KeyConditionExpression=Key('email').eq(email)
    )
    items = response['Items']
    if (len(items) == 0):
        return 0 # no user exists
    # print(items)
    if (items[0]["password"] != password):
        return 1 # incorrect password
    
    # valid user, need to give a unique id
    unique_id = urandom(randint(100,150)).hex()
    
    table.update_item(
        Key={
            'email': email
        },
        UpdateExpression='SET unique_id = :val',
        ExpressionAttributeValues={
            ':val': unique_id
        }
    )
    to_return = {
        "name": items[0]["name"],
        "email": items[0]["email"],
        "unique_id": unique_id,
        "jobs": items[0]["jobs"]
    }
    
    return to_return
