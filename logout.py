from boto3 import resource
from boto3.dynamodb.conditions import Key, Attr

def lambda_handler(event, context):
    email = event["email"]
    
    unique_id = "none"
    
    dynamodb = resource('dynamodb')
    table = dynamodb.Table('JobTracker01')
    
    response = table.query(
        KeyConditionExpression=Key('email').eq(email)
    )
    items = response['Items']
    if (len(items) == 0):
        return 0 # no user exists
    table.update_item(
        Key={
            'email': email
        },
        UpdateExpression='SET unique_id = :val',
        ExpressionAttributeValues={
            ':val': unique_id
        }
    )
    return 1 # logged out
