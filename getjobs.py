from boto3 import resource
from boto3.dynamodb.conditions import Key, Attr

def lambda_handler(event, context):
    email = event["email"]
    unique_id = event["unique_id"]
    
    dynamodb = resource('dynamodb')
    table = dynamodb.Table('JobTracker01')
    
    response = table.query(
        KeyConditionExpression=Key('email').eq(email)
    )
    items = response['Items']
    if (len(items) == 0):
        return 0 # no user exists

    if (items[0]["unique_id"] != unique_id):
        return 1 # incorrect unique_id

    to_return = {
        "name": items[0]["name"],
        "email": items[0]["email"],
        "unique_id": items[0]["unique_id"],
        "jobs": items[0]["jobs"]
    }
    return to_return