from boto3 import resource
from boto3.dynamodb.conditions import Key, Attr
from hashlib import md5

def lambda_handler(event, context):
    email = event["email"]
    unique_id = event["unique_id"]
    job_title = event["job_title"]
    job_url = event["job_url"]
    company = event["company"]
    job_id = md5((job_title + job_url).encode()).hexdigest()
    status = "Not Applied"
    important_date = None
    
    if "job_id" in event:
        job_id = event["job_id"]
    if "status" in event:
        status = event["status"]
    if "important_date" in event:
        important_date = event["important_date"]
    
    
    dynamodb = resource('dynamodb')
    table = dynamodb.Table('JobTracker01')
    
    response = table.query(
        KeyConditionExpression=Key('email').eq(email)
    )
    items = response['Items']
    if (len(items) == 0):
        return 0 # no user exists
    # print(items)
    # print(items)
    # return 10
    if (items[0]["unique_id"] != unique_id):
        return 1 # incorrect unique_id
    
    # if job_id in items[0]["jobs"]:
    #     return 2 # job exists
    
    new_job = {
        job_id : {
            "job_url": job_url,
            "job_title": job_title,
            "company": company,
            "status": status,
            "important_date": important_date
        }
    }
    
    new_job_set = {**items[0]["jobs"], **new_job}
    
    table.update_item(
        Key={
            'email': email
        },
        UpdateExpression='SET jobs = :val1',
        ExpressionAttributeValues={
            ':val1': new_job_set
        }
    )
    to_return = {
        "name": items[0]["name"],
        "email": items[0]["email"],
        "unique_id": items[0]["unique_id"],
        "jobs": new_job_set
    }
    return to_return
    
    
