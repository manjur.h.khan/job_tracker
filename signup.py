from boto3 import resource
from hashlib import sha512

from boto3.dynamodb.conditions import Key, Attr

def lambda_handler(event, context):
    email = event["email"]
    password = event["password"]
    name = event["name"]
    phone = event["phone"]
    unique_id = "none"
    password = sha512(password.encode()).hexdigest()
    
    dynamodb = resource('dynamodb')
    table = dynamodb.Table('JobTracker01')
    
    response = table.query(
        KeyConditionExpression=Key('email').eq(email)
    )
    items = response['Items']
    if (len(items) == 0):
        table.put_item(
           Item={
                'email': email,
                'password': password,
                'name': name,
                'phone': phone,
                'unique_id': unique_id,
                'jobs': {}
            }
        )

    return len(items)
